/*
 * Timer.h
 *
 *  Created on: 
 *      Author: chungnt@epi-tech.com.vn
 *
 *      
*/

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef _PTM_H_
#define _PTM_H_

#include "HT66F0031.h"
#include "HT8_Type.h"

void delayMs(u16 timeDelay);
void TM_Init(void);
void TM_CounterModeConfig(u8 TempPeriod);
void TM_EnableISR(void);
#endif
/******************* (C) COPYRIGHT 2018 Holtek Semiconductor Inc *****END OF FILE****/