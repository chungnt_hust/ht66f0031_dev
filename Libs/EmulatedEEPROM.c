/*
 * EmulatedEEPROM.c
 *
 *  Created on: May 18, 2021
 *      Author: chungnt@epi-tech.com.vn
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "EmulatedEEPROM.h"

typedef union
{
	u8 byteH;
	u8 byteL;
	u16 dat;
} eepromDat_t;

static eepromDat_t eepromDat;
static u16 testDat = 1;

u16 EmulatedEEPROM_Check(void)
{
	if(EmulatedEEPROM_Read_Word(BK_ADD) != BK_DAT)
	{
		EmulatedEEPROM_Erase(PAGE_1);
		EmulatedEEPROM_Write_Word(BK_ADD, BK_DAT);
		
		EmulatedEEPROM_Write_Word(0x02, 2048);
	}
	
	testDat = EmulatedEEPROM_Read_Word(0x02);
	return testDat;
}

/* Erase 1 page = 16words */
void EmulatedEEPROM_Erase(u8 page)
{
	/* Erasing a Data Page of the Emulated EEPROM */
	_ear = page;	
	_ecr = 0x00;		// Erase time=2ms (40H for 4ms, 80H for 8ms, C0H for 16ms)
	_emi = 0;
	_eeren = 1;			// set EEREN bit,enable	erase operation
	_eer = 1;			// start Erase Cycle, set	EER	bit, executed immediately after setting EEREN bit
	_emi = 1;
	while(_eer == 1){_clrwdt();}	// check for erase cycle end	
}

/**
  * @brief EmulatedEEPROM write function.
  * @param[in] The data you want to write to EmulatedEEPROM.
  * It can be 0x00~0x3fff.
  * @param[in] Specifies EmulatedEEPROM address.
  * It can be 0~32.
  * @retval None
  * 2page emulated eeprom
  *	Erase 16 words/page
  *	Write 1 word/time
  * Read 1 word/time
  * Page size = 16 words.
  */
void EmulatedEEPROM_Write_Word(u8 adr, u16 Data)
{	
	eepromDat.dat = Data;
	
	/* Writing Data to the Emulated EEPROM */
	_ear = adr; 		// user defined address
	_edl = eepromDat.byteH;
	_edh = eepromDat.byteL;
	
	_ecr = 0x00;		// Write time=2ms (40H for 4ms, 80H for 8ms, C0H for 16ms)
	_emi = 0;				
	_ewren = 1;			// set EWREN bit,enable	write operation
	_ewr = 1;			// start Write Cycle, set EWR bit, executed immediately after setting EWREN bit	
	_emi = 1;
	while(_ewr==1){_clrwdt();}	// check for write cycle end
}
	

/**
  * @brief EmulatedEEPROM read function.
  * @param[in] Specifies EmulatedEEPROM address that you want to read.
  * It can be 0~32.
  * @retval EEPROM data.
  */
u16 EmulatedEEPROM_Read_Word(u8 adr)
{	
	_ear = adr;					// user	defined	address
	_erden = 1;					// set ERDEN bit, enable read operation
	_erd = 1;					// start Read Cycle -set ERD bit
	while(_erd==1){_clrwdt();}			// check for read cycle end
	_ecr = 0;					// disable Emulated	EEPROM read	if no more read	
        						// operations are required       									 	 
	eepromDat.byteL = _edl;			// move	read data to register
	eepromDat.byteH = _edh;
		
	return eepromDat.dat;
}

/******************* (C) COPYRIGHT 2018 Holtek Semiconductor Inc *****END OF FILE****/