/*
 * Led7.c
 *
 *  Created on: 
 *      Author: chungnt@epi-tech.com.vn
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "Led7.h"
#include "Timer.h"

#define NUM_OF_LED 4

static const u8 Led7_Code[11] = {0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x6F, 0x00};
static u8 s_Buffer[NUM_OF_LED] = {0x3F, 0x06, 0x5B, 0x4F};
static u8 s_Index = 0;

#define SEG_ON 	0
#define SEG_OFF 1
static void _decode(u8 code)
{
	(code & 0x01) ? (_pb3 = SEG_ON):(_pb3 = SEG_OFF);
	(code & 0x02) ? (_pb4 = SEG_ON):(_pb4 = SEG_OFF);
	(code & 0x04) ? (_pb5 = SEG_ON):(_pb5 = SEG_OFF);
	(code & 0x08) ? (_pa4 = SEG_ON):(_pa4 = SEG_OFF);
	(code & 0x10) ? (_pb0 = SEG_ON):(_pb0 = SEG_OFF);
	(code & 0x20) ? (_pa6 = SEG_ON):(_pa6 = SEG_OFF);
	(code & 0x40) ? (_pa7 = SEG_ON):(_pa7 = SEG_OFF);
}

static void _offAllSeg(void)
{
	_pb3 = SEG_OFF;
	_pb4 = SEG_OFF;
	_pb5 = SEG_OFF;
	_pa4 = SEG_OFF;
	_pb0 = SEG_OFF;
	_pa6 = SEG_OFF;
	_pa7 = SEG_OFF;
}

#define DIGIT_ON 	0
#define DIGIT_OFF 	1
static void _ctrlDigit(u8 digit, bool onoff)
{
	switch(digit)
	{
		case 0: { _pb2 = onoff; break;}
		case 1: { _pb1 = onoff; break;}
		case 2: { _pa5 = onoff; break;}
		case 3: { _pa1 = onoff; break;}
	}
}

static void _ctrlOffAllDigit(void)
{
	_pb2 = DIGIT_OFF;
	_pb1 = DIGIT_OFF;
	_pa5 = DIGIT_OFF;
	_pa1 = DIGIT_OFF;
}

u8 count ;
void Led7_Init(void)
{
	_pac1 = 0;	// digit 4
	_pac5 = 0;	// digit 3
	_pbc1 = 0;	// digit 2
	_pbc2 = 0;	// digit 1
	
	_pbc3 = 0;	// A
	_pbc4 = 0;	// B
	_pbc5 = 0;	// C
	_pac4 = 0;	// D
	_pbc0 = 0;	// E
	_pac6 = 0;	// F
	_pac7 = 0;	// G
	
	_pa1 = 1;
	_pa5 = 1;
	_pb1 = 1;
	_pb2 = 1;
	
	_pb3 = 1;
	_pb4 = 1;
	_pb5 = 1;
	_pa4 = 1;
	_pb0 = 1;
	_pa6 = 1;
	_pa7 = 1;

}

void Led7_ShowNumber(u16 num)
{
	u8 i;
	for(i = 0; i < 4; i++)
	{
		s_Buffer[i] = Led7_Code[num%10];
		num = num/10;
		if(num == 0) 
		{
			i++;
			break;
		}		
	}
	for(; i < 4; i++) s_Buffer[i] = 0;
}

void Led7_IRQHandler(void)
{
	_offAllSeg();
	_ctrlOffAllDigit();
	if(++s_Index == NUM_OF_LED) s_Index = 0;
	_decode(s_Buffer[s_Index]);
	_ctrlDigit(s_Index, DIGIT_ON);
}
