/*
 * EmulatedEEPROM.h
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 *      
*/

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef _E_EEPROM_H_
#define	_E_EEPROM_H_

#include "HT66F0031.h"
#include "HT8_Type.h"

#define PAGE_1 0
#define PAGE_2 16

#define BK_ADD 0
#define BK_DAT 0x55

u16 EmulatedEEPROM_Check(void);
void EmulatedEEPROM_Erase(u8 page);
void EmulatedEEPROM_Write_Word(u8 adr, u16 Data);				//EEPROM Write mode,No interrupt
u16 EmulatedEEPROM_Read_Word(u8 adr);							//EEPROM Read mode

#endif

/******************* (C) COPYRIGHT 2018 Holtek Semiconductor Inc *****END OF FILE****/