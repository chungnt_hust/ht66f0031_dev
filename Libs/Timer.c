/*
 * Timer.c
 *
 *  Created on: 
 *      Author: chungnt@epi-tech.com.vn
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "Timer.h"
#include "Led7.h"

volatile bool g_TimerInt;

static volatile u16 s_time;

void delayMs(u16 timeDelay)
{
	s_time = timeDelay;
	while(s_time > 0);
}

/**
  * @brief timer periodic interrupt routine.
  * @par Parameters:
  * None
  * @retval
  * None
  */
void __attribute((interrupt(0x0C))) PTM_ISR(void)
{
	
	/* user define */
	_tf = 0;	
	if(s_time > 0) s_time--;
	//Led7_IRQHandler();
	
	g_TimerInt = TRUE;
	
	_emi = 1; // enable global interrupt
}

/**
  * @brief TM initialization function.
  * @param[in] Non.
  * @retval Non.
  */
void TM_Init(void)
{
/******************** work mode select ********************/
	_tm1 = 1; _tm0 = 0;	// Select TM timer/counter Mode
		
/**************** end of work mode select ****************/		

/********************* clock select **********************/
	_ts = 1;	// Timer fTP clock source selection = 32kHz
	_tpsc2 = 0; _tpsc1 = 0; _tpsc0 = 0; // fTP/1 = 32kHz
		
/********************* end of clock select **********************/
	
	TM_CounterModeConfig(255-32); // count from (255-32) to 255 = 32 count	
	TM_EnableISR();
}

/**
  * @brief TM timer/counter mode period config function.
  * @param[in] period value,
  * overflow time=TempPeriod * Tclock,
  * @retval None.
  */
void TM_CounterModeConfig(u8 TempPeriod)
{
	_tmr = TempPeriod;
}

void TM_EnableISR(void)
{	
	_te = 1; // enable timer interrupt	

	_ton = 1; // on TM counter
	_emi  = 1; // enable global interrupt
}

