/*
 * Led7.h
 *
 *  Created on: 
 *      Author: chungnt@epi-tech.com.vn
 *
 *      
*/

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef _LED7_H_
#define _LED7_H_

#include "HT66F0031.h"
#include "HT8_Type.h"

void Led7_Init(void);
void Led7_ShowNumber(u16 num);
void Led7_IRQHandler(void);

#endif
/******************* (C) COPYRIGHT 2018 Holtek Semiconductor Inc *****END OF FILE****/