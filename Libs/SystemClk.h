/*
 * SystemClk.c.h
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 *      
*/

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef _SYS_Clock_H_
#define _SYS_Clock_H_

#include "HT66F0031.h"

/****** system clock prescaler select ******/
	#define	SYSCLOCK_FH				(1)	
//	#define	SYSCLOCK_FH_DIV2		(1)	
//	#define	SYSCLOCK_FH_DIV4		(1)	
//	#define	SYSCLOCK_FH_DIV8		(1)	
//	#define	SYSCLOCK_FH_DIV16		(1)	
//	#define	SYSCLOCK_FH_DIV32		(1)	
//	#define	SYSCLOCK_FH_DIV64		(1)	
//	#define	SYSCLOCK_FLIRC			(1)	
/** end of system clock prescaler select **/

void SysClock_Init();

#endif
