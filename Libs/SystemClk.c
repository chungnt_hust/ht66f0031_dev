/*
 * SystemClk.c
 *
 *  Created on: 
 *      Author: chungnt@epi-tech.com.vn
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "SystemClk.h"


/**
  * @brief system clock prescaler select.
  * @par Parameters:
  * None
  * @retval 
  * None
  */
void SysClock_Init()
{
	_wdtc = 0b01010111; // enable watchdog timer 2^15/32k ~ 1.024s
#ifdef	SYSCLOCK_FH
	_hircen = 1;	_cks2 = 0; _cks1 = 0; _cks0 = 0;	//set Fsys as FH = 8MHz as default
	
#elif	SYSCLOCK_FH_DIV2
	_hircen = 1; _cks2 = 0; _cks1 = 0; _cks0 = 1;	//set Fsys as FH/2
	
#elif	SYSCLOCK_FH_DIV4
	_hircen = 1; _cks2 = 0; _cks1 = 1; _cks0 = 0;	//set Fsys as FH/4
	
#elif	SYSCLOCK_FH_DIV8
	_hircen = 1; _cks2 = 0; _cks1 = 1; _cks0 = 1;	//set Fsys as FH/8
	
#elif	SYSCLOCK_FH_DIV16
	_hircen = 1; _cks2 = 1; _cks1 = 0; _cks0 = 0;	//set Fsys as FH/16
	
#elif	SYSCLOCK_FH_DIV32
	_hircen = 1; _cks2 = 1; _cks1 = 0; _cks0 = 1;	//set Fsys as FH/32
	
#elif	SYSCLOCK_FH_DIV64
	_hircc = 1; _cks2 = 1; _cks1 = 1; _cks0 = 0;	//set Fsys as FH/64
	
#elif	SYSCLOCK_FLIRC
	_hircen = 0;	_cks2 = 1; _cks1 = 1; _cks0 = 1;	//set Fsys as FLIRC(32K)
#endif	

#ifndef SYSCLOCK_FLIRC
	while(_hircf == 0); // wait high speed system	oscillator SST ready flag, auto set 1 by hardware
	_emi = 0;
#endif
}

