/*
 * ADC.c
 *
 *  Created on: 
 *      Author: chungnt@epi-tech.com.vn
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "ADC.h"
#include "Led7.h"
#include "Timer.h"

#define ADC_VR (float)1.6

#if(USE_ADC_INT)
volatile bool s_convDone = FALSE;

void __attribute((interrupt(0x1H))) ADC_ISR(void)
{
	_clrwdt();
	/* user define */
	_adf = 0;	
	
	s_convDone = TRUE;
	
	/*_emi = 1; // enable global interrupt*/
}
#endif

void ADC_Init(void)
{
	_pas3 = 1;										// PA3 as AN3
	_sacks2 = 1; _sacks1 = 0; _sacks0 = 1; 			// fsys	
	_adcen = 1; 									// enable ADC 
	_sains2 = 0; _sains1 = 0; _sains0 = 0; 			// external analog channel input 
	_sacs3 = 0; _sacs2 = 0; _sacs1 = 1; _sacs0 = 1; // AN3 step 4
	/*_savrs1 = 0; _savrs0 = 1; 					// internal A/D converter power supply AVdd*/
	_savrs1 = 1; _savrs0 = 0; 						// internal A/D converter power supply Vr = 1.6
	_adrfs = 1; 									//  SADOH=D[9:8]; SADOL=D[7:0]
	
#if(USE_ADC_INT)
	// config interrupt 
	_adf = 0;
	_ade = 1;
	_emi = 1; // enable global interrupt
#endif
}

u16 ADC_Read(void)
{
	// start condition
	_start = 0; 
	_start = 1; 
	GCC_DELAY(5);
	_start = 0;
#if(USE_ADC_INT)
	s_convDone = FALSE;
	while(s_convDone == FALSE);
#else
	GCC_DELAY(5);
	while(_adbz == 1);
#endif
	return ((_sadoh << 8) | _sadol);
}
