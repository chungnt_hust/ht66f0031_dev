/*
 * ADC.h
 *
 *  Created on: 
 *      Author: chungnt@epi-tech.com.vn
 *
 *      
*/

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef _ADC_H_
#define _ADC_H_

#include "HT66F0031.h"
#include "HT8_Type.h"

void ADC_Init(void);
u16 ADC_Read(void);
#endif
/******************* (C) COPYRIGHT 2018 Holtek Semiconductor Inc *****END OF FILE****/