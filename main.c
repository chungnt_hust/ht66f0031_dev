#include "HT66F0031.h"
#include "SystemClk.h"
#include "Timer.h"
#include "Led7.h"
#include "ADC.h"
#include "EmulatedEEPROM.h"

extern volatile bool g_TimerInt;
static u16 softTimer1 = 0;

void main()
{	
	SysClock_Init();
	ADC_Init();
	TM_Init();
	Led7_Init();
	Led7_ShowNumber(EmulatedEEPROM_Check());
	
	while(1)
	{
		_clrwdt();
		
		if(g_TimerInt == TRUE)
		{
			g_TimerInt = FALSE;
			softTimer1++;
			Led7_IRQHandler();
		}
		
		if(softTimer1 > 1000)
		{
			softTimer1 = 0;
			Led7_ShowNumber(ADC_Read());
		}
	}
}